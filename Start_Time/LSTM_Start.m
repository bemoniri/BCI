clear
clc
close

cd('../dataset')
load('subject_1.mat')
cd('../Start_Time')
fs = 2400;

signals = cat(3, data.train{1}, data.train{2}, data.train{3});
responses = squeeze(signals(64,:,:));
[indexes, ~]   = find(squeeze(signals(64,:,:)));
signals = signals(1:end-1,:,:);

clear data

%% Remove DC Component

matSize   = size(signals);
mean     = reshape(repmat(squeeze(mean(signals, 2)), [1, 1, matSize(2)]), [matSize(1), matSize(2), matSize(3)]);
signals  = signals - mean;      

clear mean

%% Binary Response Function

binary_responses = zeros(size(responses));
for i = 1 : matSize(3)
    binary_responses((1:7200)>=indexes(i),i) = 1;
end

trans_responses = zeros(size(responses));
for i = 1 : matSize(3)
    trans_responses((1:7200)>=indexes(i),i) = 1;
    trans_responses((1:7200)>=indexes(i)+400,i) = 0;
end

%% Reformat Data
clc
I = randperm(matSize(3));


clear XTrain YTrain
j = 1;
for i = I(1:end-5)
   XTrain{j} = signals(:,:,i);
   YTrain{j} = binary_responses(:,i)';
   j = j + 1;
end

clear XTest YTest
j = 1;
for i = I(end-4 : end)
   XTest{j} = signals(:,:,i);
   YTest{j} = binary_responses(:,i)';
   j = j + 1;
end

 
%%
numResponses = size(YTrain{1},1);
featureDimension = size(XTrain{1},1);
numHiddenUnits = 200;

layers = [ ...
    sequenceInputLayer(featureDimension)
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    fullyConnectedLayer(50)
    dropoutLayer(0.5)
    fullyConnectedLayer(numResponses)
    regressionLayer];

maxEpochs = 100;
miniBatchSize = 20;
options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'InitialLearnRate',0.01, ...
    'GradientThreshold',1, ...
    'Shuffle','never', ...
    'Plots','training-progress',...
    'Verbose',0);

net = trainNetwork(XTrain,YTrain,layers,options);

%%

YPred = predict(net,XTest,'MiniBatchSize',1);

figure
hold on
plot(YPred{1})
plot(YTest{1})