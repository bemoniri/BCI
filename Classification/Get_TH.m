clear
clc
close
subject_id = 7;
name = ['subject_',num2str(subject_id), '.mat'];
cd('../dataset')
load(name)
cd('../Classification/')
fs = 2400;

%% Loading Subject's Data
% this is a test

signal = cat(3, data.train{1}, data.train{2}, data.train{3}, data.train{4});
signal = signal(1:63, :, :);
signal = cat(3, signal, data.test);
signal = signal(:, 1:20:end, :);
fs = fs/20;
FData.signal = signal(:,73:end,:);
% FData.signal = signal(:,1:end,:);
clear signal

label = [ones(1, size(data.train{1}, 3)), 2*ones(1, size(data.train{2}, 3)), ...
    3*ones(1, size(data.train{3}, 3)), 4*ones(1, size(data.train{4}, 3)), zeros(1, size(data.test, 3))];

%% Removing DC Component

matSize   = size(FData.signal);
mean     = reshape(repmat(squeeze(mean(FData.signal, 2)), [1, 1, matSize(2)]), [matSize(1), matSize(2), matSize(3)]);
FData.signal  = FData.signal - mean;      
clear mean

%% Feature Extraction

FData.var    =  squeeze(var(FData.signal,0, 2));       % Variance
FData.skew   =  squeeze(skewness(FData.signal,0, 2));  % Skewness
FData.RMS    =  squeeze(rms(FData.signal,2));          % RMS Value

% Mean Frequency
for channel = 1 : 63
    FData.meanFreq(channel,:)    =  meanfreq(squeeze(FData.signal(channel,:,:)), fs);
end

% Median Frequency
for channel = 1 : 63
    FData.medFreq(channel,:)    =  medfreq(squeeze(FData.signal(channel,:,:)), fs);
end

% Sine Transform
for channel = 1 : 63
    FData.DST  (channel, :, :)  =  dst(squeeze(FData.signal(channel, :, :)));
end

% Cosine Transform
for channel = 1 : 63
    FData.DCT  (channel, :, :)  =  dct(squeeze(FData.signal(channel, :, :)));
end

% Frequncy Band Filters
h_alpha = BPF(7200, 7.5  , 13.5, fs);
h_beta =  BPF(7200, 13.5 , 20, fs);
h_theta = BPF(7200, 3.5  ,  7.5, fs);
h_delta = BPF(7200, eps,  3.5, fs);


% Filtering
for channel = 1 : 63
    FData.alpha_band(channel,:,:)  = doFilt(h_alpha, squeeze(FData.signal(channel, :, :)));
    FData.beta_band (channel,:, :) = doFilt(h_beta,  squeeze(FData.signal(channel, :, :)));
    FData.theta_band(channel,:, :) = doFilt(h_theta, squeeze(FData.signal(channel, :, :)));
    FData.delta_band(channel,:, :) = doFilt(h_delta, squeeze(FData.signal(channel, :, :)));
end

clear h_alpha h_beta h_delta h_theta


FData.alphaEnergy = squeeze(sum(FData.alpha_band.^2, 2));
FData.betaEnergy  = squeeze(sum(FData.beta_band.^2, 2));
FData.thetaEnergy = squeeze(sum(FData.theta_band.^2, 2));
FData.deltaEnergy = squeeze(sum(FData.delta_band.^2, 2));


for channel = 1 : 63
    FData.STFT  (channel, :, :, :)  =  abs(STFT(squeeze(FData.signal(channel, :, :)),10,0,15,fs));
end

clear channel

% Common Spatial Patterns
covMat(1, :, :) = cov(mean(FData.signal(:,:,label == 1), 3)');
covMat(2, :, :) = cov(mean(FData.signal(:,:,label == 2), 3)');
covMat(3, :, :) = cov(mean(FData.signal(:,:,label == 3), 3)');
covMat(4, :, :) = cov(mean(FData.signal(:,:,label == 4), 3)');

spatialFilter_EXE = MulticlassCSP(squeeze(covMat), 2);

for trials = 1 : size(FData.signal, 3)
    FData.CSP1(:, trials)    = squeeze(spatialFilter_EXE(1,:))*squeeze(FData.signal(:,:,trials));
    FData.CSP2(:, trials)    = squeeze(spatialFilter_EXE(2,:))*squeeze(FData.signal(:,:,trials));
end

% Wavelet Transform
[Lo_D,Hi_D] = wfilters('bior3.5','d');
for channel = 1 : 63
    for trial = 1 : matSize(3)
    [FData.DWT_cA(channel, :, trial), FData.DWT_cd(channel, :, trial)]  = ...
        dwt(squeeze(FData.signal(channel, :, trial)),Lo_D,Hi_D);
    end
end


%% Train Feature Matrix

Feature = [];
label = [ones(1, size(data.train{1}, 3)), 2*ones(1, size(data.train{2}, 3)), ...
    3*ones(1, size(data.train{3}, 3)), 4*ones(1, size(data.train{4}, 3)), zeros(1, size(data.test, 3))];

for i = 1 : matSize(3)
     Feature(i,:) = [reshape(FData.signal(:,:,i),1,[]) ...
                    FData.var(:,i)' FData.skew(:,i)' ...
                    FData.RMS(:,i)' FData.meanFreq(:,i)' ...
                    FData.medFreq(:,i)'...
                    reshape(FData.DST(:,:,i),1,[]) ...
                    reshape(FData.DCT(:,:,i),1,[]) ...
                    reshape(FData.alpha_band(:,:,i),1,[]) ...
                    reshape(FData.beta_band(:,:,i),1,[]) ...
                    reshape(FData.theta_band(:,:,i),1,[]) ...
                    reshape(FData.delta_band(:,:,i),1,[]) ...
                    FData.alphaEnergy(:,i)'...
                    FData.betaEnergy(:,i)'...
                    FData.thetaEnergy(:,i)'...
                    FData.deltaEnergy(:,i)'...
                    reshape(FData.STFT(:,:,:,i),1,[]) ...
                    FData.CSP1(:,i)' ...
                    FData.CSP2(:,i)' ...
                    reshape(FData.DWT_cA(:,:,i),1,[]) ...
                    reshape(FData.DWT_cd(:,:,i),1,[]) ...
                    ];  
end

clear i

%% Feature Names

FeatureName = [repmat({'Signal'}, 1, length(reshape(FData.signal(:,:,1),1,[]))) ...
               repmat({'Var'}, 1, length( FData.var(:,1)' )) ...
               repmat({'Skew'}, 1, length( FData.skew(:,1)' )) ...
               repmat({'RMS'}, 1, length( FData.RMS(:,1)' )) ...
               repmat({'meanFreq'}, 1, length( FData.meanFreq(:,1)' )) ...
               repmat({'medFreq'}, 1, length( FData.medFreq(:,1)'  )) ...
               repmat({'DST'}, 1, length( reshape(FData.DST(:,:,1),1,[]))) ...
               repmat({'DCT'}, 1, length( reshape(FData.DCT(:,:,1),1,[])  )) ...
               repmat({'alphaSignal'}, 1, length( reshape(FData.alpha_band(:,:,1),1,[]) )) ...
               repmat({'betaSignal'}, 1, length( reshape(FData.beta_band(:,:,1),1,[]) )) ...
               repmat({'thetaSignal'}, 1, length( reshape(FData.theta_band(:,:,1),1,[]) )) ...
               repmat({'deltaSignal'}, 1, length( reshape(FData.delta_band(:,:,1),1,[]) )) ...
               repmat({'alphaEnergy'}, 1, length( FData.alphaEnergy(:,1)')) ...
               repmat({'betaEnergy'}, 1, length( FData.betaEnergy(:,1)')) ...
               repmat({'thetaEnergy'}, 1, length( FData.thetaEnergy(:,1)' )) ...
               repmat({'deltaEnergy'}, 1, length( FData.deltaEnergy(:,1)' )) ...
               repmat({'STFT'}, 1, length( reshape(FData.STFT(:,:,:,1),1,[]) )) ...
               repmat({'CSP1'}, 1, length( FData.CSP1(:,1)')) ...
               repmat({'CSP2'}, 1, length( FData.CSP2(:,1)')) ...
               repmat({'DWT_cA'}, 1, length( reshape(FData.DWT_cA(:,:,1),1,[]) )) ...
               repmat({'DWT_cD'}, 1, length( reshape(FData.DWT_cd(:,:,1),1,[]) )) ...

];

%% Seperating Train and Test

Train_Feature = Feature(label ~= 0, :);
Train_Label = label(label ~= 0);

Test_Feature = Feature(label == 0, :);

%% Hold-out Validating 
clear LDA_percentage SVM_percentage Linear_percentage Nb_percentage Behrad_percentage Mode_percentage Mixed_percentage

for k = 1 : 5
    clc
    k
    
    I = randperm(size(Train_Feature, 1));
    HoldOut_Test_Feature = Train_Feature(I(1:7), :);
    HoldOut_Train_Feature  = Train_Feature(I(8:end), :);
    
    HoldOut_Test_Label  = Train_Label(I(1:7));
    HoldOut_Train_Label = Train_Label(I(8:end));
    
    for i = 1 : size(HoldOut_Train_Feature, 2)
        pVal(i) = anova1(HoldOut_Train_Feature(:,i), HoldOut_Train_Label, 'off');
        if(mod(i, 10000) == 0)
            i
        end
    end
    th = logspace(-2.5,-8,40)
    for kappa = 1 : 40
        %  Classification
        %LDA
        LDA_th = th(kappa);
        LDA_Obj = fitcdiscr(HoldOut_Train_Feature(:, pVal<LDA_th), HoldOut_Train_Label);
        predict(LDA_Obj, HoldOut_Test_Feature(:,pVal<LDA_th))';
        [LDA_predicted, LDA_Score, LDA_Cost] = predict(LDA_Obj, HoldOut_Test_Feature(:,pVal<LDA_th));
        LDA_percentage(kappa, k) = 100*sum(LDA_predicted'==HoldOut_Test_Label)/size(LDA_predicted',2)

        % SVM
        SVM_th = th(kappa);
        SVM_Obj = fitcecoc(HoldOut_Train_Feature(:, pVal<SVM_th), HoldOut_Train_Label);
        predict(SVM_Obj, HoldOut_Test_Feature(:,pVal<SVM_th))';
        [SVM_predicted, SVM_Loss, ~] = predict(SVM_Obj, HoldOut_Test_Feature(:,pVal<SVM_th));
        SVM_percentage(kappa, k) = 100*sum(SVM_predicted'==HoldOut_Test_Label)/size(SVM_predicted',2)

        % Linear
        Linear_th = th(kappa);
        Linear_Obj = fitcecoc(HoldOut_Train_Feature(:, pVal<Linear_th), HoldOut_Train_Label,'learners','linear');
        predict(Linear_Obj, HoldOut_Test_Feature(:,pVal<Linear_th))';
        [Linear_predicted, Linear_Loss, ~] = predict(Linear_Obj, HoldOut_Test_Feature(:,pVal<Linear_th));
        Linear_percentage(kappa, k) = 100*sum(Linear_predicted'==HoldOut_Test_Label)/size(Linear_predicted',2)

        % Naivebayes
        Nb_th = th(kappa);
        Nb_Obj = fitcecoc(HoldOut_Train_Feature(:, pVal<Nb_th), HoldOut_Train_Label,'learners','naivebayes');
        predict(Nb_Obj, HoldOut_Test_Feature(:,pVal<Nb_th))';
        [Nb_predicted ,Nb_Loss, ~] = predict(Nb_Obj, HoldOut_Test_Feature(:,pVal<Nb_th));
        Nb_percentage(kappa, k) = 100*sum(Nb_predicted'==HoldOut_Test_Label)/size(Nb_predicted',2)


        % Behrad's Method
        NEG_COST = SVM_Loss - abs(mean(mean(SVM_Loss))*LDA_Cost);
        [~, Behrad_predicted] = max(NEG_COST');
        Behrad_percentage(kappa, k) = 100*sum(Behrad_predicted==HoldOut_Test_Label)/size(Behrad_predicted,2)

        % Mode Method
        Mode_predicted  = mode([SVM_predicted, LDA_predicted, Linear_predicted, Behrad_predicted', Nb_predicted]');
        Mode_percentage(kappa, k) = 100*sum(Mode_predicted==HoldOut_Test_Label)/size(Mode_predicted,2)
    end
end



%%  Choose th

figure
subplot(321)
semilogx(th, mean(LDA_percentage,2))
title('LDA')
subplot(322)
semilogx(th, mean(SVM_percentage,2))
title('SVM')
subplot(323)
semilogx(th, mean(Linear_percentage,2))
title('Linear')
subplot(324)
semilogx(th, mean(Mode_percentage,2))
title('Mode')
subplot(325)
semilogx(th, mean(Nb_percentage,2))
title('Naive Bayes')
subplot(326)
semilogx(th, mean(Behrad_percentage,2))
title('Mixed')

%%

TH(subject_id).SVM = 7.897e-09;
TH(subject_id).LDA = 2.031e-09;
TH(subject_id).Linear = 1.752e-11;
TH(subject_id).Nb = 0.0001061;

