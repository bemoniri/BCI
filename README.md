# NeuroTools

The repository contains the codes to classify movement usign EEG signals and detect the movement onset time. Behrad Moniri and Amirhossein Afsharrad developed all the codes provided.
Our work is available under GNU Public License V2 (GPL-2.0).


